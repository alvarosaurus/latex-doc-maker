(TeX-add-style-hook
 "doc"
 (lambda ()
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "times"
    "hyperref"))
 :latex)

